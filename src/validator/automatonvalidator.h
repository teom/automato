/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef BUCHIVALIDATOR_H
#define BUCHIVALIDATOR_H

#include <QtCore/QPointer>

#include "model/automaton.h"

class AutomatonValidator : public QObject
{
    Q_OBJECT
public:
    enum Response
    {
        Rejected,
        NotRejected,
        Accepted
    };

    enum AcceptanceCondition
    {
        Finite,
        Buechi
    };

    explicit AutomatonValidator( const QPointer<Model::Automaton> automaton,
                                 QObject *parent = 0 );
    
signals:
    
public slots:
    Response validate( const QString &string, AcceptanceCondition condition );


private:
    Response verifyFiniteCondition( const QHash< qint32, Model::State > &states,
                                    const QSet< qint32 > &currentStates );

    Response verifyBuechiCondition( const QHash<qint32, Model::State> &states,
                                    const QHash< Model::StatePair, QString > &transitions,
                                    QSet<qint32> &currentStates );


    QSet< qint32 > successorsForLetter( const QHash< Model::StatePair, QString > &transitions,
                                        qint32 stateId,
                                        const QString &string,
                                        int letterIndex );

    QSet< qint32 > allSuccessors( const QHash< Model::StatePair, QString > &transitions,
                                  qint32 stateId );

    QPointer< Model::Automaton > m_automaton;
};

#endif // BUCHIVALIDATOR_H

/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef VALIDATORWIDGET_H
#define VALIDATORWIDGET_H

#include <QtGui/QComboBox>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtCore/QPointer>
#include <QtGui/QToolBar>

#include "model/automaton.h"
#include "automatonvalidator.h"
#include "validatormenubutton.h"


class ValidatorWidget : public QToolBar
{
    Q_OBJECT
public:
    explicit ValidatorWidget( QWidget *parent = 0 );

    void setAutomaton( QPointer< Model::Automaton > automaton ) { m_automaton = automaton; }
    void unsetAutomaton() { m_automaton = 0; automatonChanged(); }
    
signals:
    
public slots:
    void automatonChanged();
private slots:
    void validate( const QString &string );
    void onAcceptanceConditionChanged( int index );

private:
    void respond( AutomatonValidator::Response response );
    QString responseToString( AutomatonValidator::Response response ) const;
    QString conditionToString( AutomatonValidator::AcceptanceCondition condition) const;
    void setWidgetEnabled( bool enabled );
    bool m_isEnabled;

    QLineEdit *m_stringEdit;
    QPointer< Model::Automaton > m_automaton;
    ValidatorMenuButton *m_validatorMenuButton;
    QLabel *m_responseLabel1;
    QLabel *m_responseLabel2;
    AutomatonValidator::AcceptanceCondition m_condition;
};

#endif // VALIDATORWIDGET_H

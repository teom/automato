/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QDebug> //TODO: remove

#include "automatonvalidator.h"

AutomatonValidator::AutomatonValidator( const QPointer< Model::Automaton > automaton,
                                        QObject *parent )
    : QObject( parent )
{
    m_automaton = automaton;
}

AutomatonValidator::Response
AutomatonValidator::validate( const QString &string, AcceptanceCondition condition )
{
    const QHash< qint32, Model::State > &states = m_automaton->states();
    const QHash< Model::StatePair, QString > &transitions = m_automaton->transitions();

    QSet< qint32 > currentStates; //initialize to initial states
    for( QHash< qint32, Model::State >::const_iterator it = states.constBegin();
         it != states.constEnd();
         ++it )
    {
        if( it.value().typeMask() & Model::State::InitialState )
            currentStates.insert( it.key() );
    }

    for( int i = 0; i < string.length(); i++ )
    {
        QSet< qint32 > newCurrentStates;
        foreach( qint32 currentStateId, currentStates )
        {
            QSet< qint32 > succ = successorsForLetter( transitions,
                                                       currentStateId,
                                                       string,
                                                       i );
            newCurrentStates.unite( succ );
        }
        currentStates.clear();
        currentStates = newCurrentStates;
    }

    switch( condition )
    {
    case Finite:
        return verifyFiniteCondition( states, currentStates );
    case Buechi:
        return verifyBuechiCondition( states, transitions, currentStates );
    }
}

AutomatonValidator::Response
AutomatonValidator::verifyFiniteCondition( const QHash<qint32, Model::State> &states,
                                           const QSet<qint32> &currentStates )
{
    Response response = Rejected;

    for( QHash< qint32, Model::State >::const_iterator it = states.constBegin();
         it != states.constEnd();
         ++it )
    {
        if( it.value().typeMask() & Model::State::AcceptingState )
        {
            if( currentStates.contains( it.key() ) ) //if one of our states at the end of
                                                     //the string is accepting
            {
                qDebug() << "FSM accepted on state " << it.value().label();
                response = Accepted;
            }
        }
    }
    return response;
}

AutomatonValidator::Response
AutomatonValidator::verifyBuechiCondition( const QHash<qint32, Model::State> &states,
                                           const QHash< Model::StatePair, QString > &transitions,
                                           QSet<qint32> &currentStates )
{
    //buechi condition
    /*
    start walking on the graph from the set of states at the end of the string without
    caring for letters, coloring visited states, and collect ACCEPT states until I can't
    find no more successors that haven't already been colored. Then I clear all colors and
    start coloring again from the colored ACCEPT states set. IF I reach an ACCEPT state I
    say NOT REJECTED and exit. IF I find a cycle before encountering a successor, I break
    that subwalk. during the walk I keep excluding from the successors queue
    the states I've already touched (colored). IF the successors queue is empty, and I
    haven't NOT-REJECTED in the meantime, I respond REJECTED.
    */
    Response response = Rejected;

    QSet< qint32 > alreadyVisited;
    QSet< qint32 > acceptStates;

    do
    {
        QSet< qint32 > newCurrentStates;
        foreach( qint32 currentStateId, currentStates )
        {
            //coloring and gathering accepting states
            alreadyVisited.insert( currentStateId );
            if( states.value( currentStateId ).typeMask() & Model::State::AcceptingState )
                acceptStates.insert( currentStateId );

            //on to successors
            QSet< qint32 > succ = allSuccessors( transitions,
                                                 currentStateId );
            foreach( qint32 avId, alreadyVisited )
            {
                if( succ.contains( avId ) ) //if this successor is already colored
                {                           //means I've found a cycle

                    succ.remove( avId );
                }
            }
            newCurrentStates.unite( succ );
        }
        currentStates.clear();
        currentStates = newCurrentStates;
    } while( !currentStates.isEmpty() );

    //If there are no accept states reachable from our initial set of end-string states,
    //we might as well skip to the end and reject. If however our reachable accept states
    //set is not empty, we need to verify if they are all in a dead end or we can reach at
    //least one *with a cycle* - only in that case we can not-reject.
    if( !acceptStates.isEmpty() )
    {
        foreach( qint32 acceptState, acceptStates )
        {
            alreadyVisited.clear();
            currentStates.clear();
            currentStates.insert( acceptState );
            do
            {
                QSet< qint32 > newCurrentStates;
                foreach( qint32 currentStateId, currentStates )
                {
                    //coloring
                    alreadyVisited.insert( currentStateId );

                    //on to successors
                    QSet< qint32 > succ = allSuccessors( transitions,
                                                         currentStateId );
                    if( succ.contains( acceptState ) )
                        return NotRejected;

                    foreach( qint32 avId, alreadyVisited )
                    {
                        if( succ.contains( avId ) ) //if this successor is already colored
                        {                           //means I've found a cycle
                            succ.remove( avId );
                        }
                    }
                    newCurrentStates.unite( succ );
                }
                currentStates.clear();
                currentStates = newCurrentStates;
            } while( !currentStates.isEmpty() );
        }
    }
    return response;
}

QSet< qint32 >
AutomatonValidator::successorsForLetter( const QHash< Model::StatePair, QString > &transitions,
                                         qint32 stateId,
                                         const QString &string,
                                         int letterIndex )
{
    QSet< qint32 > successorStates;

    for( QHash< Model::StatePair, QString >::const_iterator it = transitions.constBegin();
         it != transitions.constEnd();
         ++it )
    {
        if( it.key().first == stateId &&        //if the trans starts from this state, and
            it.value().contains( string.at( letterIndex ) ) ) //if it contains the right char
        {
            successorStates.insert( it.key().second );
        }
    }
    return successorStates;
}

QSet< qint32 >
AutomatonValidator::allSuccessors( const QHash<Model::StatePair, QString> &transitions,
                                   qint32 stateId )
{
    QSet< qint32 > successorStates;

    for( QHash< Model::StatePair, QString >::const_iterator it = transitions.constBegin();
         it != transitions.constEnd();
         ++it )
    {
        if( it.key().first == stateId &&        //if the trans starts from this state, and
            !it.value().isEmpty() )             //the transition is not empty
        {
            successorStates.insert( it.key().second );
        }
    }
    return successorStates;
}

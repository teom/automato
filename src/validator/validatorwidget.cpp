/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtCore/QDebug>
#include <QtGui/QIcon>
#include <QtGui/QHBoxLayout>
#include <QtGui/QRegExpValidator>

#include "validatorwidget.h"

ValidatorWidget::ValidatorWidget( QWidget *parent )
    : QToolBar( tr( "Validator" ), parent )
{
    m_validatorMenuButton = new ValidatorMenuButton( this );
    addWidget( m_validatorMenuButton );
    m_validatorMenuButton->addAcceptanceConditionAction( new QAction( conditionToString( AutomatonValidator::Finite ),
                                                         m_validatorMenuButton ) );
    m_validatorMenuButton->addAcceptanceConditionAction( new QAction( conditionToString( AutomatonValidator::Buechi ),
                                                         m_validatorMenuButton ) );

    m_condition = AutomatonValidator::Finite;
    connect( m_validatorMenuButton, SIGNAL( selectionChanged( int ) ),
             this, SLOT( onAcceptanceConditionChanged( int ) ) );

    addSeparator();
    QWidget *validatorFrame = new QWidget( this );
    QHBoxLayout *mainLayout = new QHBoxLayout( validatorFrame );
    validatorFrame->setLayout( mainLayout );
    m_stringEdit = new QLineEdit( validatorFrame );
    QString ttp;
    ttp += tr( "Insert here the string to be validated against the automaton.<br>" );
    ttp += tr( "The string can only contain alphanumeric characters (i.e. it must satisfy the regular expression [a-zA-Z0-9]*)." );
    ttp += tr( "<br><br>Validation starts immediately upon insertion." );
    m_stringEdit->setToolTip( ttp );
    mainLayout->addWidget( m_stringEdit );
    QRegExp rxp( "[a-zA-Z0-9]*" );
    QRegExpValidator *rxpv = new QRegExpValidator( rxp, validatorFrame );
    m_stringEdit->setValidator( rxpv );
    addWidget( validatorFrame );

    m_responseLabel1 = new QLabel( this );
    m_responseLabel1->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Expanding );
    m_responseLabel1->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
    addWidget( m_responseLabel1 );
    m_responseLabel1->setText( "Type:&nbsp;<br>Response:&nbsp;");
    m_responseLabel1->setStyleSheet( "color: #707070;");

    m_responseLabel2 = new QLabel( this );
    m_responseLabel2->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Expanding );
    m_responseLabel2->setMinimumSize( 120, 1 );
    m_responseLabel2->setAlignment( Qt::AlignLeft | Qt::AlignVCenter );
    addWidget( m_responseLabel2 );

    setWidgetEnabled( false );
}

void
ValidatorWidget::automatonChanged() //SLOT
{
    if( m_automaton )
    {
        if( m_automaton->hasInitialStates() )
        {
            if( !m_isEnabled )
            {
                setWidgetEnabled( true );
                m_stringEdit->clear();
            }
            validate( m_stringEdit->text() );
        }
        else
        {
            m_stringEdit->setText( tr( "Automaton has no initial states to start validation from." ) );
            setWidgetEnabled( false );
        }
    }
    else
    {
        m_stringEdit->setText( tr( "No automaton loaded." ) );
        setWidgetEnabled( false );
    }
}

void
ValidatorWidget::validate( const QString &string )
{
    if( m_automaton )
    {
        //We validate even if string is empty, because if the initial state is an accepting
        //state with a loop, the null string can be not-rejected.
        AutomatonValidator validator( m_automaton );
        AutomatonValidator::Response response;
        response = validator.validate( string, m_condition );

        respond( response );
    }
}

void
ValidatorWidget::onAcceptanceConditionChanged( int index )
{
    if( index > -1 )
    {
        m_condition = static_cast< AutomatonValidator::AcceptanceCondition >( index );
        automatonChanged();
    }
}

void
ValidatorWidget::respond( AutomatonValidator::Response response )
{
    QString coloredResponse = responseToString( response );
    switch( response )
    {
    case AutomatonValidator::Accepted:
        coloredResponse.prepend( "<font color=#006600>" );
        coloredResponse.append( "</font>" );
        break;
    case AutomatonValidator::NotRejected:
        coloredResponse.prepend( "<font color=#000099>" );
        coloredResponse.append( "</font>" );
        break;
    case AutomatonValidator::Rejected:
        coloredResponse.prepend( "<font color=#990000>" );
        coloredResponse.append( "</font>" );
        break;
    }

    QString rString;
    rString = conditionToString( m_condition )
            + "<br>"
            + "<b>"
            + coloredResponse
            + "</b>";
    m_responseLabel2->setText( rString );
}

QString
ValidatorWidget::responseToString( AutomatonValidator::Response response ) const
{
    QString rString;
    switch( response )
    {
    case AutomatonValidator::Rejected:
        rString = tr( "Rejected" );
        break;
    case AutomatonValidator::NotRejected:
        rString = tr( "Not rejected" );
        break;
    case AutomatonValidator::Accepted:
        rString = tr( "Accepted" );
    }
    return rString;
}

QString
ValidatorWidget::conditionToString(AutomatonValidator::AcceptanceCondition condition) const
{
    switch( condition )
    {
    case AutomatonValidator::Finite:
        return tr( "Finite state machine" );
    case AutomatonValidator::Buechi:
        return tr( "%1 automaton" ).arg( QString::fromUtf8( "Büchi" ) );
    }
    return QString();
}

void
ValidatorWidget::setWidgetEnabled( bool enabled )
{
    m_stringEdit->setEnabled( enabled );
    m_validatorMenuButton->setEnabled( enabled );
    m_responseLabel1->setEnabled( enabled );
    m_responseLabel2->clear();
    m_responseLabel2->setEnabled( enabled );

    m_isEnabled = enabled;
    if( enabled )
    {
        connect( m_stringEdit, SIGNAL( textChanged( QString ) ),
                 this, SLOT( validate( QString ) ) );
    }
    else
    {
        m_stringEdit->disconnect( this );
    }
}

/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui/QWidgetAction>
#include <QtGui/QLabel>
#include <QStyleOptionToolButton>
#include <QMouseEvent>
#include <QStylePainter>

#include "validatormenubutton.h"


ValidatorMenuButton::ValidatorMenuButton( QWidget *parent )
    : QToolButton( parent )
{
    setIcon( QIcon::fromTheme( "applications-system" ).pixmap( 32, 32 ) );
    setToolButtonStyle( Qt::ToolButtonTextBesideIcon );
    QFont f = font();
    f.setBold( true );
    setFont( f );
    setText( tr( "Validator" ) );

    m_conditionActions = new QActionGroup( this );
    m_conditionActions->setExclusive( true );
    connect( m_conditionActions, SIGNAL( triggered( QAction* ) ),
             this, SLOT( onConditionActionTriggered( QAction* ) ) );

    m_menu = new QMenu( this );
    QLabel *conditionLabel = new QLabel( tr( "Acceptance condition" ), this );
    conditionLabel->setMargin( 4 );
    conditionLabel->setAlignment( Qt::AlignBottom | Qt::AlignHCenter );
    QWidgetAction *clAction = new QWidgetAction( this );
    clAction->setDefaultWidget( conditionLabel );
    m_menu->addAction( clAction );
    m_menu->addSeparator();

    connect( this, SIGNAL( pressed() ), this, SLOT( onClicked() ) );
    connect( m_menu, SIGNAL( aboutToHide() ), this, SLOT( onMenuHidden() ) );
}

void
ValidatorMenuButton::addAcceptanceConditionAction( QAction *action )
{
    m_conditionActions->addAction( action );

    action->setCheckable( true );
    if( m_conditionActions->actions().count() == 1 )
        action->setChecked( true );

    m_menu->addAction( action );
}

void
ValidatorMenuButton::showMenu()
{
    m_menu->popup( QPoint( 0, 0 ) );
}


void
ValidatorMenuButton::onConditionActionTriggered( QAction *action ) //SLOT
{
    int index = m_conditionActions->actions().indexOf( action );
    if( index > -1 )
        emit selectionChanged( index );

}

void
ValidatorMenuButton::onClicked() //SLOT
{
    //HACK: always popup the menu above the button!
    QPoint myPos = mapToGlobal( QPoint( 0, 0 ) );
    myPos.ry() = myPos.y() - m_menu->sizeHint().height();

    m_menu->popup( myPos );
}

void
ValidatorMenuButton::onMenuHidden() //SLOT
{
    mouseReleaseEvent( new QMouseEvent( QMouseEvent::None, QPoint( 0, 0 ),
                                        Qt::LeftButton, Qt::LeftButton,
                                        Qt::KeyboardModifierMask ) );
}


void
ValidatorMenuButton::paintEvent( QPaintEvent *e )
{
    QToolButton::paintEvent( e );
    QStyleOptionToolButton opt;
    initStyleOption( &opt );
    opt.rect = QRect( opt.rect.width() - 20, 0, 20, 20 );
    QStylePainter p( this );
    p.drawPrimitive( QStyle::PE_IndicatorArrowUp, opt );
}

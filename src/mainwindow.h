/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QAction>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGraphicsView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QSlider>
#include <QtGui/QToolBar>

#include "view/automatonscene.h"
#include "view/automatonstate.h"
#include "validator/validatorwidget.h"

class StateChoiceWidget;
class QWidgetAction;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow( QWidget *parent = 0 );
    
signals:

public slots:
    void onItemInserted( View::AutomatonState *item );
    void onItemSelected( QGraphicsItem *item );
    void onSelectionCleared();
    
private slots:
    void refreshTitleBar();
    void showAboutDialog();
    void showAboutQtDialog();
    void setAutomataToolsEnabled( bool );
    void setStateToolsVisible( bool );
    void onPointerToolsGroupClicked();
    void changeZoomScale( int );
    void onChoiceWidgetStateChanged( int typeMask );
    void editItem();

    void fileNew();
    void fileSave();
    void fileSaveAs();
    void fileClose();
    void fileOpen();
    void markFileDirty();

protected:
    void closeEvent( QCloseEvent *e );
    
private:
    void createToolBar();
    void createItemMenu();
    void createBottomBar();

    QString m_currentFileName;
    bool    m_isFileOpen;
    bool    m_isFileDirty;

    //Main toolbar and actions
    QToolBar *m_mainToolbar;
    QAction *m_newAction;
    QAction *m_openAction;
    QAction *m_saveAction;
    QAction *m_saveAsAction;
    QAction *m_closeAction;

    QActionGroup *m_pointerToolsGroup;
    QAction *m_pointerAction;
    QAction *m_connectorAction;
    QAction *m_addStateAction;

    QWidgetAction *m_stateChoiceAction;
    StateChoiceWidget *m_stateChoiceWidget;
    QSlider *m_zoomSlider;
    QLabel *m_zoomBox;

    QAction *m_deleteAction;
    QAction *m_editAction;

    //View and scene
    View::AutomatonScene *m_scene;
    QGraphicsView *m_view;
    QMenu *m_itemMenu;
    QPointer< Model::Automaton > m_automaton;
    ValidatorWidget *m_validatorWidget;
};

#endif // MAINWINDOW_H

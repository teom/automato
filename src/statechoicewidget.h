/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STATECHOICEWIDGET_H
#define STATECHOICEWIDGET_H

#include <QtCore/QDebug>
#include <QtGui/QWidget>

class QCheckBox;

class StateChoiceWidget : public QWidget
{
    Q_OBJECT
public:
    explicit StateChoiceWidget( QWidget *parent = 0 );

    int stateTypeMask() const;

public slots:
    void setVisible(bool visible) { QWidget::setVisible( visible); }
    void setTypeMask( int typeMask );

signals:
    void stateChanged( int stateTypeMask );
    void choiceChanged( int stateTypeMask );
    
private slots:
    void onCheckBoxesClicked();

private:
    int m_typeMask;
    QCheckBox *m_isInitialStateCheckBox;
    QCheckBox *m_isAcceptingStateCheckBox;
    
};

#endif // STATECHOICEWIDGET_H

/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STATE_H
#define STATE_H

#include <QtCore/QMetaType>
#include <QtCore/QObject>
#include <QtCore/QPointF>

//These must be forward-declared for non-member operators.
namespace Model { class State; }
QDataStream & operator<<( QDataStream &out, const Model::State &state );
QDataStream & operator>>( QDataStream &in, Model::State &state );

namespace Model
{

class State : public QObject
{
    Q_OBJECT
public:
    enum Type  //NOTE: non-exclusive, binary mask!
    {
        DefaultState = 0,
        AcceptingState,
        InitialState
    };

    State( QObject *parent = 0 );
    State( const State& other );
    ~State() {}
    State &operator=( const State &other );
    bool operator==( const State &other ) const;

    const QPointF &pos() const { return m_pos; }
    void setPos( const QPointF &pos ) { m_pos = pos; emit stateChanged( this, false ); }

    const QString &label() const { return m_label; }
    void setLabel( const QString &label );

    int typeMask() const { return m_typeMask; }
    void setTypeMask( int typeMask );


signals:
    void stateChanged( State *me, bool semanticsChanged = true );

private:
    QString m_label;
    int m_typeMask;
    QPointF m_pos;

    //Serialization! These are declared outside the class and friended here.
    friend QDataStream & ::operator<<( QDataStream &out, const Model::State &state );
    friend QDataStream & ::operator>>( QDataStream &in, Model::State &state );
};

} //namespace Model

#endif // STATE_H

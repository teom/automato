/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef AUTOMATON_H
#define AUTOMATON_H

#include <QtCore/QObject>
#include <QtCore/QHash>

#include "state.h"


//These must be forward-declared for non-member operators.
namespace Model { class Automaton; }
QDataStream & operator<<( QDataStream &out, const Model::Automaton &automaton );
QDataStream & operator>>( QDataStream &in, Model::Automaton &automaton );

namespace Model
{

typedef QPair< qint32, qint32 > StatePair; //also because my keyboard doesn't have dedicated <>
                                     //keys so it's easier this way to avoid stuff like
                                     //AltGr+Shift+{Z,X}

static inline StatePair makeStatePair( int start, int end )
{ return qMakePair( start, end ); }

class Automaton : public QObject
{
    Q_OBJECT
public:
    explicit Automaton( QObject *parent = 0 );

    /**
     * @brief reportAllItemsToView this method goes through all the items and reports
     *                             their presence to the view, if any, by emitting
     *                             stateAdded and transitionAdded signals for all of them.
     * @warning IT SHOULD ONLY BE CALLED AFTER LOADING THE AUTOMATON FROM FILE!
     */
    void reportAllItemsToView();

    bool hasInitialStates() const;

    const QHash< qint32, State > &states() const { return m_states; }
    const QHash< StatePair, QString > &transitions() const { return m_transitions; }
    
signals:
    void stateAdded( int id );
    void stateRemoved( int id );
    void stateChanged( int id );
    void transitionAdded( Model::StatePair pair );   //need to prefix namespace because of
    void transitionRemoved( Model::StatePair pair ); //string matching in signal-slot
    void transitionChanged( Model::StatePair pair ); //connections :(

    void anythingChanged(); //Convenience signal, only use it to mark file as dirty or validator.
    void semanticsChanged();
    
public slots:
    void addState( int typeMask, const QPointF &position );
    void removeState( int id );

    void addTransition( int startStateId, int endStateId );
    void removeTransition( Model::StatePair id );

    /**
     * @brief state returns a volatile pointer to the State object corresponding to the
     *              given id.
     * @param id the id.
     * @return the requested State object, or a default-contstructed value if there is no
     *         state for that id.
     */
    State &state( int id );

    /**
     * @brief stringForTransition returns the comma-separated list of characters
     *                            recognized by a given transition.
     * @param pair the pair of ids corresponding to the starting and ending states of the
     *             transition.
     * @return the comma-separated list of allowed characters, or the empty string if
     *         there is no such transition.
     */
    QString stringForTransition( StatePair pair ) const;

    /**
     * @brief setStringForTransition sets a comma-separated list of characters to be
     *                               recognized by a transition.
     * @param pair the pair of ids corresponding to the starting and ending states of the
     *             transition.
     * @param acceptedChars the list of chars to be recognized, as a QString.
     * @warning If there is no such transition, the method exits silently.
     */
    void setStringForTransition( StatePair pair, QString acceptedChars );

private slots:
    /**
     * @brief onSingleStateChanged checks for validity and emits stateChanged( int )
     * @warning NEVER connect this slot to anything that isn't a State
     */
    void onSingleStateChanged( State *state , bool semanticsChanged );

private:
    QHash< qint32, State > m_states;
    QHash< StatePair, QString > m_transitions;
    int m_stateCounter;

    //Serialization! These are declared outside the class and friended here.
    friend QDataStream & ::operator<<( QDataStream &out, const Model::Automaton &automaton );
    friend QDataStream & ::operator>>( QDataStream &in, Model::Automaton &automaton );
};

}

#endif // AUTOMATON_H

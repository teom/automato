/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "state.h"
#include <QtCore/QDebug>

namespace Model
{

State::State( QObject *parent )
    : QObject( parent )
    , m_typeMask( 0 )
{
}

State::State( const State &other )
    : m_label( other.m_label )
    , m_typeMask( other.m_typeMask )
    , m_pos( other.m_pos )
{
}

State &
State::operator=( const State &other )
{
    m_label = other.m_label;
    m_typeMask = other.m_typeMask;
    m_pos = other.m_pos;
    return *this;
}

bool
State::operator==( const State &other ) const
{
    if( m_label    == other.m_label &&
        m_typeMask == other.m_typeMask &&
        m_pos      == other.m_pos )
        return true;
    else
        return false;
}

void
State::setLabel( const QString &label )
{
    if( m_label != label )
    {
        m_label = label;
        emit stateChanged( this );
    }
}

void
State::setTypeMask( int typeMask )
{
    if( m_typeMask != typeMask )
    {
        m_typeMask = typeMask;
        emit stateChanged( this );
    }
}

} //namespace Model


QDataStream &
operator<<( QDataStream &out, const Model::State &state )
{
    out << state.m_label;
    out << static_cast< qint32 >( state.m_typeMask );
    out << state.m_pos;
    return out;
}

QDataStream &
operator>>( QDataStream &in, Model::State &state )
{
    in >> state.m_label;
    qint32 typeMask;
    in >> typeMask;
    state.m_typeMask = static_cast< int >( typeMask );
    in >> state.m_pos;
    return in;
}

/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "automaton.h"

#include <QtCore/QDebug>

namespace Model
{

Automaton::Automaton( QObject *parent )
    : QObject( parent )
    , m_stateCounter( 0 )
{
    connect( this, SIGNAL( stateAdded( int ) ),   this, SIGNAL( anythingChanged() ) );
    connect( this, SIGNAL( stateChanged( int ) ), this, SIGNAL( anythingChanged() ) );
    connect( this, SIGNAL( stateRemoved( int ) ), this, SIGNAL( anythingChanged() ) );
    connect( this, SIGNAL( transitionAdded( Model::StatePair ) ),   this, SIGNAL( anythingChanged() ) );
    connect( this, SIGNAL( transitionChanged( Model::StatePair ) ), this, SIGNAL( anythingChanged() ) );
    connect( this, SIGNAL( transitionRemoved( Model::StatePair ) ), this, SIGNAL( anythingChanged() ) );

    connect( this, SIGNAL( stateAdded( int ) ),   this, SIGNAL( semanticsChanged() ) );
    //stateChanged is already forwarded to semanticsChanged only if the operation isn't just a move
    connect( this, SIGNAL( stateRemoved( int ) ), this, SIGNAL( semanticsChanged() ) );
    connect( this, SIGNAL( transitionAdded( Model::StatePair ) ),   this, SIGNAL( semanticsChanged() ) );
    connect( this, SIGNAL( transitionChanged( Model::StatePair ) ), this, SIGNAL( semanticsChanged() ) );
    connect( this, SIGNAL( transitionRemoved( Model::StatePair ) ), this, SIGNAL( semanticsChanged() ) );

}

void Automaton::reportAllItemsToView()
{
    for( QHash< qint32, State >::iterator it = m_states.begin();
         it != m_states.end();
         ++it )
    {
        connect( &( it.value() ), SIGNAL( stateChanged( State *, bool ) ),
                 this, SLOT( onSingleStateChanged( State *, bool ) ) );
        emit stateAdded( it.key() );
    }

    for( QHash< StatePair, QString >::iterator it = m_transitions.begin();
         it != m_transitions.end();
         ++it )
    {
        emit transitionAdded( it.key() );
    }
}

bool
Automaton::hasInitialStates() const
{
    foreach( State s, m_states )
    {
        if( s.typeMask() & State::InitialState )
            return true;
    }
    return false;
}

State &
Automaton::state( int id )
{
    return m_states[ id ];
}

QString
Automaton::stringForTransition( StatePair pair ) const
{
    return m_transitions.value( pair, QString() );
}

void
Automaton::setStringForTransition( StatePair pair, QString acceptedChars )
{
    if( m_transitions.contains( pair ) )
        m_transitions[ pair ] = acceptedChars;
    emit transitionChanged( pair );
}

void
Automaton::onSingleStateChanged( State *state, bool semChanged ) //SLOT
{
    if( state )
    {
        int id = m_states.key( *state, -1 );
        if( id > -1 )
        {
            emit stateChanged( id );
            if( semChanged )
                emit semanticsChanged();
        }
        qDebug() << "State " << state->label() << " in model.\n" << state->pos()
                 << "  typemask:" << state->typeMask();
    }
}

void
Automaton::addState( int typeMask, const QPointF &position ) //SLOT
{
    State state;
    state.setPos( position );
    state.setLabel( QString( "q%1" ).arg( QString::number( m_stateCounter ) ) );
    state.setTypeMask( typeMask );
    m_states.insert( m_stateCounter, state );
    connect( &m_states[ m_stateCounter ], SIGNAL( stateChanged( State *, bool ) ),
             this, SLOT( onSingleStateChanged( State *, bool ) ) );
    emit stateAdded( m_stateCounter++ );
}

void
Automaton::removeState( int id ) //SLOT
{
    m_states.remove( id );
    emit stateRemoved( id );
}

void
Automaton::addTransition( int startStateId, int endStateId )
{
    StatePair p = makeStatePair( startStateId, endStateId );
    if( !m_transitions.contains( p ) ) //disallow duplicate transitions!
    {
        m_transitions.insert( makeStatePair( startStateId, endStateId ), QString() );
        emit transitionAdded( p );
    }
}

void
Automaton::removeTransition( StatePair id ) //SLOT
{
    m_transitions.remove( id );
    emit transitionRemoved( id );
}

} //namespace Model


//QDataStream serialization operators.
QDataStream &
operator<<( QDataStream &out, const Model::Automaton &automaton )
{
    out << automaton.m_states;
    out << automaton.m_transitions;
    out << static_cast< qint32 >( automaton.m_stateCounter );
    return out;
}

QDataStream &
operator>>( QDataStream &in, Model::Automaton &automaton )
{
    automaton.m_states.clear();
    in >> automaton.m_states;
    automaton.m_transitions.clear();
    in >> automaton.m_transitions;
    qint32 stateCounter;
    in >> stateCounter;
    automaton.m_stateCounter = static_cast< int >( stateCounter );
    return in;
}

/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui/QPainter>
#include <QtGui/QPen>
#include <QtCore/qmath.h> //brings in math.h as well
#include <QtGui/QGraphicsSceneMouseEvent>

#include "automatonstate.h"
#include "automatontransition.h"
#include "automatonscene.h"

namespace View
{

AutomatonTransition::AutomatonTransition( AutomatonState *startItem,
                                          AutomatonState *endItem ,
                                          const QString &label,
                                          QGraphicsItem *parent,
                                          QGraphicsScene *scene )
    : QGraphicsPathItem( parent, scene )
    , m_startItem( startItem )
    , m_endItem( endItem )
    , m_color( Qt::black )
    , m_label( label )
{
    setFlag( QGraphicsItem::ItemIsSelectable, true );
    setPen( QPen( m_color, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

    m_labelItem = new QGraphicsSimpleTextItem( this, scene );

    redraw();
}

QRectF
AutomatonTransition::boundingRect() const
{
    qreal extra = ( pen().width() + 20 ) / 2.;

    QRectF rect( shape().boundingRect() );
    rect = rect.normalized()
               .adjusted( -extra, -extra, extra, extra );
    return rect;
}

QPainterPath
AutomatonTransition::shape() const
{
    QPainterPath path = QGraphicsPathItem::shape(); //we get the line
    path.addPolygon( m_arrowPoly ); //and add the arrow head
    return path;
}

void
AutomatonTransition::updatePosition() //called by the scene!
{
    update();
}

void
AutomatonTransition::setLabel( const QString &label )
{
    m_label = label;
    redraw();
}

void
AutomatonTransition::paint( QPainter *painter,
                                const QStyleOptionGraphicsItem *option,
                                QWidget *widget )
{
    painter->setBrush( Qt::transparent );

    QLineF absoluteLine( mapFromItem( m_startItem, 0, 0 ),
                         mapFromItem( m_endItem,   0, 0 ) );
    QPainterPath path;

    if( m_startItem == m_endItem ) //special case: arc onto self
    {
        QPointF itemCenter( mapFromItem( m_startItem , 0, 0 ) );
        qreal eWidth = 60;
        qreal eHeight = 80;
        QPointF ellipseCorner( itemCenter.x() - eWidth / 2,
                               itemCenter.y() - ( eHeight + 20 ) );
        path.addEllipse( ellipseCorner.x(),
                         ellipseCorner.y(),
                         eWidth,
                         eHeight );

        const qreal lPadding = 6;

        m_labelItem->setPos( itemCenter.x() - m_labelItem->boundingRect().width() / 2.,
                             ellipseCorner.y() - lPadding - m_labelItem->boundingRect().height() );
        m_labelItem->update();
    }
    else //different start and end ==> we draw a real arc
    {
        QPointF midPoint = QPointF( ( absoluteLine.p1().rx() + absoluteLine.p2().rx() ) / 2.,
                                    ( absoluteLine.p1().ry() + absoluteLine.p2().ry() ) / 2. );
        qreal arcHeight = 200.;

        qreal angle = qAcos( absoluteLine.dx() / absoluteLine.length() );
        if( absoluteLine.dy() >= 0 )
            angle = ( M_PI * 2 ) - angle; //flip the arc if needed

        QPointF oldMid = midPoint;
        midPoint = midPoint + QPointF( qSin( angle + M_PI ) * arcHeight,
                                       qCos( angle + M_PI ) * arcHeight );

        path.moveTo( absoluteLine.p1() );
        path.quadTo( midPoint, absoluteLine.p2() );

        const qreal lPadding = 18;

        QPointF labelCenter = oldMid + QPointF( qSin( angle + M_PI ) * ( arcHeight / 2. + lPadding ),
                                                qCos( angle + M_PI ) * ( arcHeight / 2. + lPadding ) );

        m_labelItem->setPos( labelCenter.x() - m_labelItem->boundingRect().width() / 2.,
                             labelCenter.y() - m_labelItem->boundingRect().height() / 2. );
        m_labelItem->update();
    }

    // let's build and draw the arrow!
    qreal arrowHeadSize = 20.;

    QPointF realEndPoint;
    qreal arrowAngle;

    if( m_startItem == m_endItem )
    {
        const qreal arrowPercent = .071; //determined manually - that's ok since the loop
                                         //won't move or resize
        realEndPoint = path.pointAtPercent( arrowPercent );
        arrowAngle = ( path.angleAtPercent( arrowPercent ) * M_PI ) / 180.; //in radians this time
        arrowAngle -= M_PI; //need to rotate it
    }
    else
    {
        QLineF mainLine = QLineF::fromPolar( 500., path.angleAtPercent( 1. ) - 180. );
        mainLine.translate( mapFromItem( m_endItem, 0, 0 ) ); //this is the tangent at the end
                                                              //of the curve

        QPolygonF endItemShape = m_endItem->shape().toFillPolygon();

        // Find the position where to draw the arrow head and where to attach the rest of the
        // transition arrow!
        QPointF p1 = endItemShape.first() + m_endItem->pos();
        QPointF p2;
        QPointF r;
        QLineF endLine;
        //intersect the mainLine with the shape of the end item
        for( int i = 1; i < endItemShape.count(); ++i )
        {
            p2 = endItemShape.at( i ) + m_endItem->pos();
            endLine = QLineF( p1, p2 );
            QLineF::IntersectType intersectType =
                endLine.intersect( mainLine, &r ); //r := intersection point with the main line
            if( intersectType == QLineF::BoundedIntersection )
                break;
            p1 = p2;
        }

        endLine = QLineF( r, mapFromItem( m_endItem, 0, 0 ) ); //cut the mainLine at r
        qreal arrowPercent = //close enough; this is the % of the curve where we'll draw the arrow!
                path.percentAtLength( path.length() - endLine.length() );

        // Now let's figure out the angle of the arrow relative to the x axis, and rotate the
        // arrow head.
        arrowAngle = ( path.angleAtPercent( arrowPercent ) * M_PI ) / 180.; //in radians this time
        arrowAngle -= M_PI; //we need to rotate it!

        realEndPoint = path.pointAtPercent( arrowPercent );
    }


    QPointF headP1 = realEndPoint +
                     QPointF( qSin( arrowAngle + M_PI / 3 ) * arrowHeadSize,
                              qCos( arrowAngle + M_PI / 3 ) * arrowHeadSize );
    QPointF headP2 = realEndPoint +
                     QPointF( qSin( arrowAngle + M_PI - M_PI / 3 ) * arrowHeadSize,
                              qCos( arrowAngle + M_PI - M_PI / 3 ) * arrowHeadSize );

    m_arrowPoly.clear();
    m_arrowPoly << realEndPoint << headP1 << headP2; //fill in the new points

    //Finally, we draw the path and the arrow!
    setPath( path ); //for boundingRect
    painter->drawPath( path );

    QPen p = pen();
    p.setColor( m_color );
    painter->setPen( p );
    painter->setBrush( m_color ); //need to color the arrow!
    painter->drawPolygon( m_arrowPoly );

    QGraphicsPathItem::paint( painter, option, widget );
}

void
AutomatonTransition::mouseDoubleClickEvent( QGraphicsSceneMouseEvent *event )
{
    AutomatonScene *s = qobject_cast< AutomatonScene * >( scene() );
    s->editSelectedItem();
    event->accept();
}

void
AutomatonTransition::redraw()
{
    m_labelItem->setBrush( Qt::black );
    m_labelItem->setText( m_label );

    if( m_label.isEmpty() )
    {
        m_labelItem->setBrush( Qt::gray );
        m_labelItem->setText( QString::fromUtf8( "\u2205" ) ); //UTF8 char for the empty
                                                               //set sign!
    }

    QFont labelFont = m_labelItem->font();
    labelFont.setItalic( true );
    labelFont.setPointSize( 12 );
    m_labelItem->setFont( labelFont );

    update(); //this will also position the text
}

} //namespace View

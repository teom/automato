/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUTOMATONVIEWITEM_H
#define AUTOMATONVIEWITEM_H

#include <QtGui/QGraphicsPathItem>
#include <QtGui/QMenu>
#include <QtGui/QPolygonF>

class QGraphicsSceneContextMenuEvent;

namespace View
{

class AutomatonTransition;

class AutomatonState : public QGraphicsPathItem
{
public:
    enum
    {
        Type = UserType + 15    //just so we can identify it
    };

    explicit AutomatonState( int typeMask,
                             QMenu *itemMenu,
                             QGraphicsItem *parent = 0,
                             QGraphicsScene *scene = 0 );

    void addTransition( AutomatonTransition *trans );
    void removeTransition( AutomatonTransition *trans );
    void removeTransitions();
    //unfortunately I do need a copy, luckily QList does implicit sharing
    QList< AutomatonTransition * > transitions() const { return m_myTransitions; }

    int typeMask() const { return m_typeMask; }
    void setTypeMask( int typeMask );

    void setLabel( const QString &label );

    QPolygonF polygon() const   { return m_polygon; }

    QPixmap image() const;
    int type() const { return Type; }

protected:
    void contextMenuEvent( QGraphicsSceneContextMenuEvent *event );
    void mouseDoubleClickEvent( QGraphicsSceneMouseEvent *event );
    QVariant itemChange( GraphicsItemChange change, const QVariant &value );

private:
    void redraw();

    QGraphicsSimpleTextItem *m_labelItem;
    QColor m_lineColor;
    QColor m_fillColor;
    int m_typeMask;
    QPolygonF m_polygon;
    QMenu *m_itemMenu;
    QString m_label;
    QList< AutomatonTransition * > m_myTransitions;
};

} //namespace View

#endif // AUTOMATONVIEWITEM_H

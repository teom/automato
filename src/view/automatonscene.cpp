/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QLineF>
#include <QtGui/QGraphicsSceneMouseEvent>
#include <QtGui/QInputDialog>
#include <QtGui/QPen>
#include <QtCore/QDebug> //FIXME: remove

#include "automatonscene.h"
#include "automatontransition.h"
#include "model/state.h"

namespace View
{

AutomatonScene::AutomatonScene( QMenu *itemMenu, QObject *parent )
    : QGraphicsScene( parent )
    , m_itemMenu( itemMenu )
    , m_currentMode( MoveItem )
    , m_color( Qt::black )
    , m_automaton( 0 )
{
    m_currentItemType = Model::State::DefaultState;
    m_currentTransitionLine = 0;

    connect( this, SIGNAL( selectionChanged() ),
             this, SLOT( onSelectionChanged() ) );
}

void
AutomatonScene::reset() //SLOT
{
    m_states.clear();
    m_transitions.clear();
    foreach( QGraphicsItem *item, items() )
        delete item;
    items().clear();
    m_currentMode = MoveItem;
    m_color = Qt::black;
    m_currentItemType = Model::State::DefaultState;
    m_currentTransitionLine = 0;
}

void
AutomatonScene::deleteSelectedItem() //SLOT
{
    foreach( QGraphicsItem *item, selectedItems() )
    {
        deleteItem( item );
    }
}

void
AutomatonScene::deleteItem( QGraphicsItem *item ) //SLOT
{
    if( m_automaton && item )
    {
        if( item->type() == AutomatonState::Type )
        {
            AutomatonState *state =
                    qgraphicsitem_cast< AutomatonState * >( item );
            int id = m_states.key( state );
            QList< AutomatonTransition * > transitions;
            transitions.append( state->transitions() );
            state->removeTransitions();
            while( !transitions.isEmpty() )
            {
                AutomatonTransition *trans = transitions.first();
                m_automaton->removeTransition( m_transitions.key( trans ) );
                transitions.removeAll( trans );
            }

            m_automaton->removeState( id );
        }
        else if( item->type() == AutomatonTransition::Type )
        {
            AutomatonTransition *trans =
                    qgraphicsitem_cast< AutomatonTransition * >( item );
            Model::StatePair id = m_transitions.key( trans );
            m_automaton->removeTransition( id );
        }
        else
        {
            removeItem( item ); //this should NOT happen
        }
    }
}

void
AutomatonScene::changeSelectedStateType( int typeMask ) //SLOT
{
    foreach( QGraphicsItem *item, selectedItems() )
    {
        if( item->type() == AutomatonState::Type )
        {
            AutomatonState *state =
                    qgraphicsitem_cast< AutomatonState * >( item );
            if( m_automaton )
            {
                m_automaton->state( m_states.key( state ) ).setTypeMask( typeMask );
            }
        }
    }
}

void
AutomatonScene::loadAutomaton( Model::Automaton *automaton ) //SLOT
{
    m_automaton = automaton;
    connect( m_automaton, SIGNAL( stateAdded( int ) ),
             this,        SLOT( addState( int ) ) );//continue
    connect( m_automaton, SIGNAL( stateRemoved( int ) ),
             this,        SLOT( removeState( int ) ) );
    connect( m_automaton, SIGNAL( stateChanged( int ) ),
             this,        SLOT( updateState( int ) ) );
    connect( m_automaton, SIGNAL( transitionAdded( Model::StatePair ) ),
             this,        SLOT( addTransition( Model::StatePair ) ) );
    connect( m_automaton, SIGNAL( transitionRemoved( Model::StatePair ) ),
             this,        SLOT( removeTransition( Model::StatePair ) ) );
    connect( m_automaton, SIGNAL( transitionChanged( Model::StatePair ) ),
             this,        SLOT( updateTransition( Model::StatePair ) ) );
}

void
AutomatonScene::unloadAutomaton()
{
    m_automaton->disconnect( this );
    reset();
}

void
AutomatonScene::addState(int id)
{
    if( m_automaton )
    {
        const Model::State &state = m_automaton->state( id );
        AutomatonState *item = new AutomatonState( state.typeMask(), m_itemMenu );
        m_states.insert( id, item ); //must happen before the following
        item->setPos( state.pos() );
        item->setLabel( state.label() );
        addItem( item );
        emit itemInserted( item );
    }
}

void
AutomatonScene::removeState( int id )
{
    AutomatonState *state = m_states.value( id );
    if( state )
    {
        removeItem( state );
        m_states.remove( id );
        delete state;
    }
}

void
AutomatonScene::updateState( int id )
{
    if( m_automaton )
    {
        const Model::State &state = m_automaton->state( id );
        AutomatonState *item = m_states.value( id );
        item->setTypeMask( state.typeMask() );
        item->setLabel( state.label() );
        emit itemChanged( item );
    }
}

void
AutomatonScene::addTransition( Model::StatePair pair )
{
    AutomatonState *startItem = m_states.value( pair.first );
    AutomatonState *endItem = m_states.value( pair.second );

    AutomatonTransition *trans = new AutomatonTransition( startItem, endItem, "" );
    startItem->addTransition( trans );
    endItem->addTransition( trans );
    trans->setZValue( -1000.0 ); //must not be on top of state items
    addItem( trans );
    m_transitions.insert( pair, trans );
    trans->updatePosition();
    trans->setLabel( m_automaton->stringForTransition( pair ) );
}

void
AutomatonScene::removeTransition( Model::StatePair pair )
{
    AutomatonTransition *trans = m_transitions.value( pair );
    if( trans )
    {
        trans->startItem()->removeTransition( trans );
        trans->endItem()->removeTransition( trans );
        removeItem( trans );
        m_transitions.remove( pair );
        delete trans;
    }
}

void
AutomatonScene::updateTransition( Model::StatePair pair )
{
    if( m_automaton )
    {
        QString string = m_automaton->stringForTransition( pair );
        AutomatonTransition *trans = m_transitions.value( pair );
        trans->setLabel( string );
        emit itemChanged( trans );
    }
}

void
AutomatonScene::editSelectedItem()
{
    if( !selectedItems().empty() && m_automaton )
    {
        if( selectedItems().first()->type() == AutomatonState::Type )
        {
            AutomatonState *state =
                    qgraphicsitem_cast< AutomatonState * >( selectedItems().first() );

            int id = m_states.key( state );
            QString currentLabel = m_automaton->state( id ).label();
            QString newLabel;
            bool ok;
            //                                ↓ I know this is very bad, now get off my lawn :)
            newLabel = QInputDialog::getText( qobject_cast< QWidget * >( parent() ),
                                              tr( "Edit State Label" ),
                                              tr( "State label:" ),
                                              QLineEdit::Normal,
                                              currentLabel,
                                              &ok );
            if( ok )
                m_automaton->state( id ).setLabel( newLabel );

        }
        else if( selectedItems().first()->type() == AutomatonTransition::Type )
        {
            AutomatonTransition *trans =
                    qgraphicsitem_cast< AutomatonTransition * >( selectedItems().first() );

            Model::StatePair id = m_transitions.key( trans );
            QString currentLabel = m_automaton->stringForTransition( id );
            QString newLabel = currentLabel;
            QRegExp rxp( "[a-zA-Z0-9](,[a-zA-Z0-9])*" );
            do
            {
                bool ok;
                //                                ↓ I know this is very bad, now get off my lawn :)
                newLabel = QInputDialog::getText( qobject_cast< QWidget * >( parent() ),
                                                  tr( "Edit Transition Letters" ),
                                                  tr( "Transition leters as a <b>comma-separated list of alphanumeric characters</b>:" ),
                                                  QLineEdit::Normal,
                                                  newLabel,
                                                  &ok );
                if( !ok )
                    return;
            } while( !rxp.exactMatch( newLabel ) );

            m_automaton->setStringForTransition( id, newLabel );
        }
        else { /*ouch*/ }
    }
}

void
AutomatonScene::onStateMoved( AutomatonState *state )
{
    if( state && m_automaton )
    {
        int id = m_states.key( state );
        m_automaton->state( id ).setPos( state->pos() );
    }
}

void
AutomatonScene::mousePressEvent( QGraphicsSceneMouseEvent *event )
{
    if( event->button() != Qt::LeftButton )
        return;

    if( m_currentMode == InsertItem )
    {
        if( m_automaton )
            m_automaton->addState( m_currentItemType, event->scenePos() );
                //program flow goes to the model, will get back here for drawing
    }
    else if( m_currentMode == InsertTransition )
    {
        m_currentTransitionLine = new QGraphicsLineItem( QLineF( event->scenePos(),
                                                                 event->scenePos() ) );
        m_currentTransitionLine->setPen( QPen( m_color, 2 ) );
        addItem( m_currentTransitionLine ); //this one is not a real transition, it's just
                                            //a line while dragging
    }
    QGraphicsScene::mousePressEvent( event );
}

void
AutomatonScene::mouseMoveEvent( QGraphicsSceneMouseEvent *event )
{
    if( m_currentMode == InsertTransition && m_currentTransitionLine != 0 )
    {
        QLineF newTransition( m_currentTransitionLine->line().p1(), event->scenePos() );
        m_currentTransitionLine->setLine( newTransition ); //still not the real transition
                                                           //just following the pointer
    }
    else if( m_currentMode == MoveItem )
    {
        QGraphicsScene::mouseMoveEvent( event ); //QGS does movement of items on its own
    }
}

void
AutomatonScene::mouseReleaseEvent( QGraphicsSceneMouseEvent *event )
{
    if( m_currentTransitionLine != 0 && m_currentMode == InsertTransition )
    {
        QList< QGraphicsItem * > startItems = items( m_currentTransitionLine->line().p1() );
        if( startItems.count() && startItems.first() == m_currentTransitionLine )
            startItems.removeFirst();

        QList< QGraphicsItem * > endItems = items( m_currentTransitionLine->line().p2() );
        if( endItems.count() && endItems.first() == m_currentTransitionLine )
            endItems.removeFirst();

        removeItem( m_currentTransitionLine );
        delete m_currentTransitionLine;
        m_currentTransitionLine = 0; //so we got rid of the temporary line

        // Check for valid AutomatonViewItem QGV items for the endpoints of the drag when
        // the mouse button is released.
        // Then we create an AutomatonViewTransition item between those coordinates, and
        // we add it to the scene.
        if( startItems.count() > 0 && endItems.count() > 0 )
        {
            QGraphicsItem *startQGItem = startItems.first();
            QGraphicsItem *endQGItem   = endItems.first();

            AutomatonState *startItem = 0;
            AutomatonState *endItem   = 0;

            //what if we clicked from a child of a state item - we need to account for that
            if( startQGItem->type() != AutomatonState::Type &&
                startQGItem->parentItem() &&
                startQGItem->parentItem()->type() == AutomatonState::Type    )
                startQGItem = startQGItem->parentItem();

            if( endQGItem->type() != AutomatonState::Type &&
                endQGItem->parentItem() &&
                endQGItem->parentItem()->type() == AutomatonState::Type    )
                endQGItem = endQGItem->parentItem();

            if( startQGItem->type() == AutomatonState::Type )
                startItem = qgraphicsitem_cast< AutomatonState * >( startQGItem );

            if( endQGItem->type() == AutomatonState::Type )
                endItem = qgraphicsitem_cast< AutomatonState * >( endQGItem );

            if( startItem && endItem )
            {
                int startId = m_states.key( startItem );
                int endId   = m_states.key( endItem );

                if( m_automaton )
                    m_automaton->addTransition( startId, endId ); //program flow goes to model
                                                                  //we draw later, on signal
            }
        }
    }
    m_currentTransitionLine = 0;
    QGraphicsScene::mouseReleaseEvent( event ); //do whatever
}

void
AutomatonScene::onSelectionChanged() //SLOT
{
    if( selectedItems().isEmpty() )
        emit selectionCleared();
    else
        emit itemSelected( selectedItems().first() );
}

bool
AutomatonScene::isItemChanged( int type )
{
    foreach( QGraphicsItem *item, selectedItems() )
    {
        if( item->type() == m_currentItemType ) //if we found a selected item of this type
            return true;
    }
    return false;
}

} //namespace View

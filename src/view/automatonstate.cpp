/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QDebug>
#include <QtCore/qmath.h>
#include <QtGui/QPainter>
#include <QtGui/QPainterPath>
#include <QtGui/QGradient>
#include <QtGui/QGraphicsScene>
#include <QtGui/QGraphicsSceneContextMenuEvent>

#include "automatonstate.h"
#include "automatontransition.h"
#include "automatonscene.h"
#include "model/state.h"

namespace View
{

AutomatonState::AutomatonState( int typeMask,
                                QMenu *itemMenu,
                                QGraphicsItem *parent ,
                                QGraphicsScene *scene )
    : QGraphicsPathItem( parent )
    , m_lineColor( Qt::black )
    , m_fillColor( QColor( 255, 250, 220 ) )
    , m_typeMask( typeMask )
    , m_itemMenu( itemMenu )
    , m_label( "" )
{
    m_labelItem = new QGraphicsSimpleTextItem( this, scene );

    redraw();

    setFlag( QGraphicsItem::ItemIsMovable, true );
    setFlag( QGraphicsItem::ItemIsSelectable, true );
    setFlag( QGraphicsItem::ItemSendsGeometryChanges, true );
}

void
AutomatonState::addTransition( AutomatonTransition *trans )
{
    m_myTransitions.append( trans );
}


void
AutomatonState::removeTransition( AutomatonTransition *trans )
{
    if( trans && m_myTransitions.contains( trans ) )
    {
        int transIndex = m_myTransitions.indexOf( trans );
        if( transIndex > -1 )
        {
            m_myTransitions.removeAt( transIndex );
        }
    }
}

void
AutomatonState::removeTransitions()
{
    while( !m_myTransitions.isEmpty() )
    {
        AutomatonTransition *trans = m_myTransitions.first();
        if( trans )
        {
            if( ( trans->startItem() == trans->endItem() ) &&
                ( trans->startItem() == this ) )
            {
                m_myTransitions.removeAll( trans );
            }
            else if( trans->startItem() == this )
            {
                trans->endItem()->removeTransition( trans );
                m_myTransitions.removeAll( trans );
            }
            else if( trans->endItem() == this )
            {
                trans->startItem()->removeTransition( trans );
                m_myTransitions.removeAll( trans );
            }
            else
            {
                qDebug() << "big bada boom!";
            }
        }
    }
}

void
AutomatonState::setTypeMask( int typeMask )
{
    m_typeMask = typeMask;
    redraw();
}

void
AutomatonState::setLabel( const QString &label )
{
    m_label = label;
    redraw();
}

QPixmap
AutomatonState::image() const
{
    QPixmap pixmap( 126, 126 );
    pixmap.fill( Qt::transparent );
    QPainter painter( &pixmap );
    painter.setPen( QPen( Qt::black, 2 ) );
    painter.translate( 63, 63 );
    painter.drawPath( shape() );
    return pixmap;
}

void
AutomatonState::contextMenuEvent( QGraphicsSceneContextMenuEvent *event )
{
    scene()->clearSelection();
    setSelected( true );
    m_itemMenu->exec( event->screenPos() );
}

void
AutomatonState::mouseDoubleClickEvent( QGraphicsSceneMouseEvent *event )
{
    AutomatonScene *s = qobject_cast< AutomatonScene * >( scene() );
    s->editSelectedItem();
    event->accept();
}

QVariant
AutomatonState::itemChange( QGraphicsItem::GraphicsItemChange change,
                            const QVariant &value )
{
    if( change == QGraphicsItem::ItemPositionChange && scene() )
    {
        foreach( AutomatonTransition *trans, m_myTransitions )
        {
            trans->updatePosition();
        }
        qobject_cast< AutomatonScene * >( scene() )->onStateMoved( this );
    }
    return value;
}

void
AutomatonState::redraw()
{
    QPainterPath path;

    path.addEllipse( -50, -50, 100, 100 );

    if( m_typeMask == Model::State::DefaultState )
    {
        m_fillColor = QColor( 255, 230, 120 );
    }

    if( m_typeMask & Model::State::InitialState )
    {
        path.moveTo( -50, 0 );
        path.lineTo( -66, 8 );
        path.lineTo( -62, 0 );
        path.lineTo( -66, -8 );
        path.closeSubpath();
        m_fillColor = QColor( 255, 160, 160);
}

    if( m_typeMask & Model::State::AcceptingState )
    {
        path.addEllipse( -42, -42, 84, 84 );
        m_fillColor = QColor( 160, 255, 160 );
    }

    path.setFillRule( Qt::WindingFill );
    setPath( path );
    setPen( QPen( m_lineColor ) );

    QLinearGradient gradient( 0, 0, 0, 1 );
    gradient.setCoordinateMode( QGradient::ObjectBoundingMode );
    gradient.setColorAt( 0, m_fillColor.lighter( 125 ) );
    gradient.setColorAt( 1, m_fillColor );

    QBrush brush( gradient );
    setBrush( brush );

    m_polygon = path.toFillPolygon();

    m_labelItem->setText( m_label );
    QFont labelFont = m_labelItem->font();
    labelFont.setBold( true );
    labelFont.setPointSize( 12 );
    m_labelItem->setFont( labelFont );
    m_labelItem->setPos( -m_labelItem->boundingRect().width() / 2.,
                         -m_labelItem->boundingRect().height() / 2. );
}

} //namespace View

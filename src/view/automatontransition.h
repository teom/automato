/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef AUTOMATONVIEWTRANSITION_H
#define AUTOMATONVIEWTRANSITION_H

#include <QGraphicsPathItem>

namespace View
{

class AutomatonState;

class AutomatonTransition : public QGraphicsPathItem
{
public:
    enum
    {
        Type = UserType + 4
    };

    AutomatonTransition( AutomatonState *startItem,
                         AutomatonState *endItem,
                         const QString &label,
                         QGraphicsItem *parent = 0,
                         QGraphicsScene *scene = 0 );

    int type() const { return Type; }

    QRectF boundingRect() const;

    /**
     * @brief shape returns a QPainterPath that describes the shape of the transition item
     * @return the QPainterPath that describes the transition.
     */
    QPainterPath shape() const;
    AutomatonState *startItem() const { return m_startItem; }
    AutomatonState *endItem() const   { return m_endItem;   }

    /**
     * @brief updatePosition refreshes the start and end points of the line.
     */
    void updatePosition();

    void setLabel( const QString &label );

protected:
    /**
     * @brief paint reimplemented to paint an arrow rather than just a line.
     * @see QGraphicsLineItem::paint
     */
    void paint( QPainter *painter,
                const QStyleOptionGraphicsItem *option,
                QWidget *widget = 0 );
    void mouseDoubleClickEvent( QGraphicsSceneMouseEvent *event );

private:
    //Basing this on QGraphicsLineItem, let's make it easy...
    void redraw();
    AutomatonState *m_startItem;
    AutomatonState *m_endItem;
    QGraphicsSimpleTextItem *m_labelItem;
    QColor m_color;
    QString m_label;
    QPolygonF m_arrowPoly; //to draw the arrow head of the transition
};

} //namespace View

#endif // AUTOMATONVIEWTRANSITION_H

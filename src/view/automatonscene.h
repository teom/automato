/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef AUTOMATONSCENE_H
#define AUTOMATONSCENE_H

#include <QtCore/QPointer>
#include <QtCore/QHash>
#include <QtGui/QGraphicsScene>
#include <QtGui/QMenu>

#include "automatonstate.h"
#include "model/automaton.h"

class QGraphicsSceneMouseEvent;

namespace View
{

class AutomatonScene : public QGraphicsScene
{
    Q_OBJECT
public:
    enum Mode
    {
        MoveItem = 0,
        InsertTransition,
        InsertItem
    };

    explicit AutomatonScene( QMenu *itemMenu, QObject *parent = 0 );
    
signals:
    void itemInserted( View::AutomatonState *item ); //QObject::connect string matching!
    void itemChanged( QGraphicsItem *item );
    void itemSelected( QGraphicsItem *item );
    void selectionCleared();
    
public slots:
    void reset();
    void setMode( Mode mode ) { m_currentMode = mode; }
    void setItemType( int typeMask ) { m_currentItemType = typeMask; }
    /**
     * @brief deleteItem deletes the selected item from the scene.
     */
    void deleteSelectedItem();
    void deleteItem( QGraphicsItem *item );
    void changeSelectedStateType( int typeMask );

    void loadAutomaton( Model::Automaton *automaton );
    void unloadAutomaton();

    void addState( int id );
    void removeState( int id );
    void updateState( int id );
    void addTransition( Model::StatePair pair );
    void removeTransition( Model::StatePair pair );
    void updateTransition( Model::StatePair pair );
    void editSelectedItem();
    void onStateMoved( AutomatonState *state );

protected:
    void mousePressEvent( QGraphicsSceneMouseEvent *event );
    void mouseMoveEvent( QGraphicsSceneMouseEvent *event );
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event );

private slots:
    void onSelectionChanged();

private:
    bool isItemChanged( int type );
    bool *m_leftMouseButtonPressed;
    //NOTE: A mouse click can give three actions: move, insert, arrow connection between
    //      items (transition). This depends on Mode m_currentMode.

    int m_currentItemType;
    QMenu *m_itemMenu;
    Mode m_currentMode;
    QPointF m_initPoint;
    QGraphicsLineItem *m_currentTransitionLine;
    QColor m_color;
    QPointer< Model::Automaton > m_automaton;
    QHash< int, View::AutomatonState * > m_states;
    QHash< Model::StatePair, View::AutomatonTransition * > m_transitions;
};

} //namespace View

#endif // AUTOMATONSCENE_H

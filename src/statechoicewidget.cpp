/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui/QCheckBox>
#include <QtGui/QVBoxLayout>

#include "statechoicewidget.h"
#include "model/state.h"

StateChoiceWidget::StateChoiceWidget( QWidget *parent )
    : QWidget( parent )
    , m_typeMask( 0 )
{
    setLayout( new QVBoxLayout( this ) );
    m_isInitialStateCheckBox = new QCheckBox( tr( "Initial State" ), this );
    layout()->addWidget( m_isInitialStateCheckBox );
    m_isInitialStateCheckBox->setFocusPolicy( Qt::NoFocus );
    m_isAcceptingStateCheckBox = new QCheckBox( tr( "Accepting State" ), this );
    layout()->addWidget( m_isAcceptingStateCheckBox );
    m_isAcceptingStateCheckBox->setFocusPolicy( Qt::NoFocus );
    layout()->setMargin( 0 );

    connect( m_isAcceptingStateCheckBox, SIGNAL( clicked( bool ) ),
             this, SLOT( onCheckBoxesClicked() ) );
    connect( m_isInitialStateCheckBox, SIGNAL( clicked( bool ) ),
             this, SLOT( onCheckBoxesClicked() ) );
}


void
StateChoiceWidget::onCheckBoxesClicked() //SLOT
{
    int typeMask = 0;
    if( m_isAcceptingStateCheckBox->isChecked() )
        typeMask |= Model::State::AcceptingState;
    if( m_isInitialStateCheckBox->isChecked() )
        typeMask |= Model::State::InitialState;

    if( m_typeMask != typeMask )
    {
        m_typeMask = typeMask;
        emit stateChanged( typeMask );
    }
}


void
StateChoiceWidget::setTypeMask( int typeMask ) //SLOT
{
    if( m_typeMask != typeMask )
    {
        m_typeMask = typeMask;
        if( typeMask & Model::State::AcceptingState )
            m_isAcceptingStateCheckBox->setChecked( true );
        else
            m_isAcceptingStateCheckBox->setChecked( false );

        if( typeMask & Model::State::InitialState )
            m_isInitialStateCheckBox->setChecked( true );
        else
            m_isInitialStateCheckBox->setChecked( false );

        emit choiceChanged( typeMask );
    }
}

/*
    Copyright 2012 Teo Mrnjavac <teo@kde.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QDebug>
#include <QtGui/QApplication>
#include <QtGui/QCheckBox>
#include <QtGui/QFileDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QMenu>
#include <QtGui/QMessageBox>
#include <QtGui/QToolButton>
#include <QtGui/QWidgetAction>
#include <QtGui/QCloseEvent>

#include "mainwindow.h"
#include "globaldeclarations.h"
#include "statechoicewidget.h"
#include "model/automaton.h"

MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , m_isFileOpen( false )
    , m_isFileDirty( false )
    , m_scene( 0 ) //must to be null on startup and assigned later in the ctor
    , m_view( 0 )
    , m_automaton( 0 )
{
    resize( 800, 600 );

    setWindowIcon( QIcon( ":/img/tomato.png" ) );

    createToolBar();
    createItemMenu();   //yields valid m_itemMenu

    m_scene = new View::AutomatonScene( m_itemMenu, this );

    m_view = new QGraphicsView( m_scene, this );
    m_view->setRenderHint( QPainter::Antialiasing, true );

    setCentralWidget( m_view );

    m_scene->setSceneRect( QRectF( 0, 0, 2000, 2000 ) );

    connect( m_scene, SIGNAL( itemInserted( View::AutomatonState * ) ),
             this, SLOT( onItemInserted( View::AutomatonState * ) ) );
    connect( m_scene, SIGNAL( itemSelected( QGraphicsItem * ) ),
             this, SLOT( onItemSelected( QGraphicsItem * ) ) );
    connect( m_scene, SIGNAL( selectionCleared() ),
             this, SLOT( onSelectionCleared() ) );

    connect( m_deleteAction, SIGNAL( triggered() ),
             m_scene, SLOT( deleteSelectedItem() ) );
    connect( m_editAction, SIGNAL( triggered() ),
             this, SLOT( editItem() ) );

    setAutomataToolsEnabled( false );
    setStateToolsVisible( false );
    setUnifiedTitleAndToolBarOnMac( true );
    refreshTitleBar();
    createBottomBar();
}

void
MainWindow::onItemInserted( View::AutomatonState *item ) //SLOT
{
    m_pointerToolsGroup->actions().at( static_cast< int >( View::AutomatonScene::MoveItem ) )
            ->setChecked( true );
    m_scene->setMode( View::AutomatonScene::MoveItem );
}

void
MainWindow::onItemSelected( QGraphicsItem *item ) //SLOT
{
    setStateToolsVisible( true );
    if( item->type() == View::AutomatonState::Type )
    {
        View::AutomatonState *state =
                qgraphicsitem_cast< View::AutomatonState * >( item );

        m_stateChoiceWidget->setTypeMask( state->typeMask() );
    }
    else
    {
        m_stateChoiceAction->setVisible( false );
    }
}

void
MainWindow::onSelectionCleared() //SLOT
{
    setStateToolsVisible( false );
}

void
MainWindow::refreshTitleBar() //SLOT
{
    QString titleBarString = AUTOMATO_APPLICATION_NAME;
    QString dirtyStar;
    if( m_isFileDirty )
        dirtyStar = " *";
    else
        dirtyStar.clear();

    if( m_isFileOpen )
    {
        if( !m_currentFileName.isEmpty() )
        {
            QFileInfo info( m_currentFileName );
            QString justName = info.fileName();
            titleBarString = justName + dirtyStar + " - " + titleBarString;
        }
        else
        {
            titleBarString = tr( "New Automaton" ) + dirtyStar + " - " + titleBarString;
        }
    }
    setWindowTitle( titleBarString );
}

void
MainWindow::showAboutDialog() //SLOT
{
    QString description;
    description = QString( "<font size=16><b>" )
                + AUTOMATO_APPLICATION_NAME + " " + AUTOMATO_VERSION
                + "</b></font><br><br>"
                + tr( "Automato is an automaton simulator. It supports creating and "
                      "editing "
                      "automata and validating strings against them as finite state "
                      "machines or as %1 automata, both deterministic and "
                      "non-deterministic." ).arg( QString::fromUtf8( "Büchi" ) )
                + "<br><br>"
                + "<a href=\"" + AUTOMATO_ORGANIZATION_DOMAIN + "\">"
                + AUTOMATO_ORGANIZATION_DOMAIN + "</a>"
                + "<br><br>"
                + tr( "For bug reports please contact the author at %1." )
                    .arg( "<a href=\"mailto:teo@kde.org\">teo@kde.org</a>" )
                + "<br><br><br>"
                + tr( "Copyright %1 2012 %2" )
                    .arg( QString::fromUtf8( "\u00A9" ) )
                    .arg( AUTOMATO_ORGANIZATION_NAME )
                + "<br><br>"
                + tr( "This program is free software: you can redistribute it and/or modify<br>"
                      "it under the terms of the GNU General Public License as published by<br>"
                      "the Free Software Foundation, either version 3 of the License, or<br>"
                      "(at your option) any later version.<br><br>"
                      "This program is distributed in the hope that it will be useful,<br>"
                      "but WITHOUT ANY WARRANTY; without even the implied warranty of<br>"
                      "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the<br>"
                      "GNU General Public License for more details.<br><br>"
                      "You should have received a copy of the GNU General Public License<br>"
                      "along with this program. If not, see %1." )
                    .arg( "<a href=\"http://www.gnu.org/licenses/\">"
                          "http://www.gnu.org/licenses/</a>" );

    QMessageBox::about( this,
                        tr( "About %1" ).arg( AUTOMATO_APPLICATION_NAME),
                        description );
}

void
MainWindow::showAboutQtDialog() //SLOT
{
    QMessageBox::aboutQt( this );
}

void
MainWindow::setAutomataToolsEnabled( bool enabled ) //SLOT
{
    m_saveAction->setEnabled( enabled );
    m_saveAsAction->setEnabled( enabled );
    m_closeAction->setEnabled( enabled );
    m_pointerAction->setEnabled( enabled );
    m_connectorAction->setEnabled( enabled );
    m_addStateAction->setEnabled( enabled );
    m_stateChoiceWidget->setEnabled( enabled );
    m_zoomSlider->setEnabled( enabled );
    m_zoomBox->setEnabled( enabled );
    m_deleteAction->setEnabled( enabled );
    m_editAction->setEnabled( enabled );
    m_view->setEnabled( enabled );

    m_pointerAction->setChecked( true ); //just a bit of resetting
}

void
MainWindow::setStateToolsVisible( bool visible ) //SLOT
{
    m_deleteAction->setVisible( visible );
    m_editAction->setVisible( visible );

    m_stateChoiceAction->setVisible( visible );
}

void
MainWindow::onPointerToolsGroupClicked() //SLOT
{
    if( m_scene && m_scene->selectedItems().isEmpty() )
        setStateToolsVisible( false );

    View::AutomatonScene::Mode mode;
    if( m_pointerToolsGroup->checkedAction() == m_connectorAction )
    {
        mode = View::AutomatonScene::InsertTransition;
    }
    else if( m_pointerToolsGroup->checkedAction() == m_addStateAction )
    {
        mode = View::AutomatonScene::InsertItem;
        m_scene->clearSelection();
        m_stateChoiceAction->setVisible( true );
    }
    else
    {
        mode = View::AutomatonScene::MoveItem;
    }
    m_scene->setMode( mode );
}

void
MainWindow::changeZoomScale( int zoomPercentage ) //SLOT
{
    if( m_view )
    {
        double newScale = zoomPercentage / 100.0;
        QMatrix oldMatrix = m_view->matrix();
        m_view->resetMatrix();
        m_view->translate( oldMatrix.dx(), oldMatrix.dy() );
        m_view->scale( newScale, newScale );
    }
}

void
MainWindow::onChoiceWidgetStateChanged( int typeMask ) //SLOT
{
    // First we need to change the m_addStateAction icon
    View::AutomatonState item( typeMask, m_itemMenu );
    QIcon icon( item.image() );
    m_addStateAction->setIcon( icon );

    // Then, if an item in the QGScene is selected, we forward to the controller.
    if( m_scene )
    {
        m_scene->setItemType( typeMask );
        if( !m_scene->selectedItems().isEmpty() )
        {
            m_scene->changeSelectedStateType( typeMask );
        }
    }
}

void
MainWindow::editItem()
{
    if( m_scene )
    {
        m_scene->editSelectedItem();
    }
}

void
MainWindow::fileNew()
{
    if( m_isFileOpen )
        fileClose();
    m_automaton = new Model::Automaton( this );
    m_scene->loadAutomaton( m_automaton );
    m_validatorWidget->setAutomaton( m_automaton );
    setAutomataToolsEnabled( true );
    m_isFileOpen = true;
    connect( m_automaton, SIGNAL( anythingChanged() ),
             this, SLOT( markFileDirty() ) );
    connect( m_automaton, SIGNAL( anythingChanged() ),
             m_validatorWidget, SLOT( automatonChanged() ) );

    m_validatorWidget->automatonChanged();
    m_isFileDirty = false;
    refreshTitleBar();
}

void
MainWindow::fileSave()
{
    if( m_isFileOpen )
    {
        if( m_currentFileName.isEmpty() )
        {
            fileSaveAs();
            return;
        }
        else
        {
            QFile file( m_currentFileName, this );
            bool ok = file.open( QIODevice::WriteOnly );
            if( ok )
            {
                QDataStream out( &file );
                const Model::Automaton *automaton = m_automaton.data();
                out << *automaton;
                file.close();

                m_isFileDirty = false;
                refreshTitleBar();
            }
            else
            {
                QMessageBox::critical( this,
                                       tr( "Error saving file" ),
                                       tr( "The file you are trying to save cannot be written to disk.\nPlease retry in a different location.\n\nIf you think this should not be happening, please contact the author to report a bug at %1." )
                                            .arg( AUTOMATO_ORGANIZATION_DOMAIN ),
                                       QMessageBox::Ok,
                                       QMessageBox::Ok );
            }
        }
    }
}

void MainWindow::fileSaveAs()
{
    if( m_isFileOpen )
    {
        QString filePath = QFileDialog::getSaveFileName( this,
                                                         tr( "Save Automaton As" ),
                                                         tr( "MyAutomaton.automato" ),
                                                         tr( "Automato automaton files (*.automato);;All files (*.*)" ),
                                                         0,
                                                         0 );
        if( !filePath.isEmpty() )
        {
            m_currentFileName = filePath;
            fileSave();
        }
    }
}

void
MainWindow::fileClose()
{
    if( m_isFileDirty )
    {
        QMessageBox::StandardButton response =
                QMessageBox::question( this,
                                       tr( "Confirm close" ),
                                       tr( "The current automaton has unsaved changes.\nDo you wish to save the automaton before closing?" ),
                                       QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel,
                                       QMessageBox::Save );

        if( response & QMessageBox::Cancel )
            return;

        if( response & QMessageBox::Save )
            fileSave();
    }
    m_scene->unloadAutomaton();
    m_automaton->deleteLater();
    m_validatorWidget->unsetAutomaton();
    setAutomataToolsEnabled( false );
    m_isFileDirty = false;
    m_isFileOpen = false;
    m_currentFileName.clear();
    refreshTitleBar();
}

void
MainWindow::fileOpen()
{
    if( m_isFileOpen )
    {
        fileClose();
    }
    QString filePath = QFileDialog::getOpenFileName( this,
                                                     tr( "Open Automaton" ),
                                                     "",
                                                     tr( "Automato automaton files (*.automato);;All files (*.*)" ),
                                                     0,
                                                     0 );
    if( filePath.isEmpty() )
    {
        return;
    }
    else
    {
        QFile file( filePath, this );
        bool ok = file.open( QIODevice::ReadOnly );
        if( ok )
        {
            QDataStream in( &file );
            Model::Automaton *automaton = new Model::Automaton( this );
            in >> *automaton;
            file.close();

            if( in.status() == QDataStream::Ok )
            {
                m_automaton = automaton;
                m_scene->loadAutomaton( m_automaton );
                m_validatorWidget->setAutomaton( m_automaton );
                m_automaton->reportAllItemsToView();
                setAutomataToolsEnabled( true );
                m_isFileOpen = true;
                connect( m_automaton, SIGNAL( anythingChanged() ),
                         this, SLOT( markFileDirty() ) );
                connect( m_automaton, SIGNAL( semanticsChanged() ),
                         m_validatorWidget, SLOT( automatonChanged() ) );
                m_validatorWidget->automatonChanged();

                m_isFileDirty = false;
                m_currentFileName = filePath;
                refreshTitleBar();
            }
            else
            {
                QMessageBox::critical( this,
                                       tr( "File opening file" ),
                                       tr( "The file you are trying to open seems to be corrupt.\nPlease retry with a different file.\n\nIf you think this should not be happening, please contact the author to report a bug at %1." )
                                            .arg( AUTOMATO_ORGANIZATION_DOMAIN ),
                                       QMessageBox::Ok,
                                       QMessageBox::Ok );
            }
        }
        else
        {
            QMessageBox::critical( this,
                                   tr( "Error opening file" ),
                                   tr( "The file you are trying to open cannot be read from disk.\nPlease retry with a different file.\n\nIf you think this should not be happening, please contact the author to report a bug at %1." )
                                        .arg( AUTOMATO_ORGANIZATION_DOMAIN ),
                                   QMessageBox::Ok,
                                   QMessageBox::Ok );
        }
    }
}

void
MainWindow::markFileDirty() //SLOT
{
    if( !m_isFileDirty )
    {
        m_isFileDirty = true;
        refreshTitleBar();
    }
}

void
MainWindow::createToolBar()
{
    m_mainToolbar = new QToolBar( tr( "Main Toolbar" ), this );
    m_mainToolbar->setFloatable( false );
    m_mainToolbar->setMovable( false );
    m_mainToolbar->setIconSize( QSize( 32, 32 ) );


    addToolBar( Qt::TopToolBarArea, m_mainToolbar );



    QToolButton *menuButton = new QToolButton( this );
    menuButton->setIcon( QIcon::fromTheme( "preferences-other" ) );
    menuButton->setText( tr( "Automaton" ) );
    QFont menuButtonFont = menuButton->font();
    menuButtonFont.setBold( true );
    menuButton->setFont( menuButtonFont );
    menuButton->setToolButtonStyle( Qt::ToolButtonTextBesideIcon );
    m_mainToolbar->addWidget( menuButton );
    menuButton->setPopupMode( QToolButton::InstantPopup );

    QMenu *toolsMenu = new QMenu( tr( "Tools" ), this );
    menuButton->setMenu( toolsMenu );

    m_newAction = new QAction( QIcon::fromTheme( "document-new" ),
                               tr( "&New..." ),
                               this );
    m_newAction->setShortcuts( QKeySequence::New );
    toolsMenu->addAction( m_newAction );
    connect( m_newAction, SIGNAL( triggered() ),
             this, SLOT( fileNew() ) );

    m_openAction = new QAction( QIcon::fromTheme( "document-open" ),
                                tr( "&Open..." ),
                                this );
    m_openAction->setShortcuts( QKeySequence::Open );
    toolsMenu->addAction( m_openAction );
    connect( m_openAction, SIGNAL( triggered() ),
             this, SLOT( fileOpen() ) );

    toolsMenu->addSeparator();

    m_closeAction = new QAction( QIcon::fromTheme( "document-close" ),
                                tr( "&Close" ),
                                this );
    m_closeAction->setShortcuts( QKeySequence::Close );
    toolsMenu->addAction( m_closeAction );
    connect( m_closeAction, SIGNAL( triggered() ),
             this, SLOT( fileClose() ) );

    toolsMenu->addSeparator();

    m_saveAction = new QAction( QIcon::fromTheme( "document-save" ),
                                tr( "&Save" ),
                                this );
    m_saveAction->setShortcuts( QKeySequence::Save );
    toolsMenu->addAction( m_saveAction );
    connect( m_saveAction, SIGNAL( triggered() ),
             this, SLOT( fileSave() ) );

    m_saveAsAction = new QAction( QIcon::fromTheme( "document-save-as" ),
                                tr( "Save As..." ),
                                this );
    m_saveAsAction->setShortcuts( QKeySequence::SaveAs );
    toolsMenu->addAction( m_saveAsAction );
    connect( m_saveAsAction, SIGNAL( triggered() ),
             this, SLOT( fileSaveAs() ) );


    toolsMenu->addSeparator();

    QAction *aboutAction = new QAction( QIcon::fromTheme( "help-about" ),
                                        tr( "About %1" ).arg( AUTOMATO_APPLICATION_NAME),
                                        this );
    toolsMenu->addAction( aboutAction );
    connect( aboutAction, SIGNAL( triggered() ),
             this, SLOT( showAboutDialog() ) );

    QAction *aboutQtAction = new QAction( QIcon( QPixmap( ":/img/qt-logo.png" ) ),
                                          tr( "About Qt" ),
                                          this );
    toolsMenu->addAction( aboutQtAction );
    connect( aboutQtAction, SIGNAL( triggered() ),
             this, SLOT( showAboutQtDialog() ) );

    toolsMenu->addSeparator();

    QAction *quitAction = new QAction( QIcon::fromTheme( "application-exit" ),
                                       tr( "Exit" ),
                                       this );
    quitAction->setShortcuts( QKeySequence::Quit );
    toolsMenu->addAction( quitAction );
    connect( quitAction, SIGNAL( triggered() ),
             QApplication::instance(), SLOT( quit() ) );


    m_mainToolbar->addSeparator();

    m_pointerToolsGroup = new QActionGroup( this );
    m_pointerToolsGroup->setExclusive( true );

    m_pointerAction = new QAction( QIcon( QPixmap( ":/img/pointer.png" ) ),
                                   tr( "&Pointer Tool" ),
                                   this );
    m_pointerAction->setShortcut( QKeySequence( tr( "Ctrl+P", "Pointer Tool" ) ) );
    m_pointerAction->setCheckable( true );
    m_pointerAction->setChecked( true );
    m_pointerToolsGroup->addAction( m_pointerAction );

    m_connectorAction = new QAction( QIcon( QPixmap( ":/img/linepointer.png" ) ),
                                     tr( "T&ransition Connector Tool" ),
                                     this );
    m_connectorAction->setShortcut( QKeySequence( tr( "Ctrl+R", "Transition Connector Tool" ) ) );
    m_connectorAction->setCheckable( true );
    m_pointerToolsGroup->addAction( m_connectorAction );

    m_addStateAction = new QAction( QIcon::fromTheme( "list-add" ),
                                    tr( "Add New S&tate Tool" ),
                                    this );
    m_addStateAction->setShortcut( QKeySequence( tr( "Ctrl+T", "Add New State Tool" ) ) );
    m_addStateAction->setCheckable( true );
    m_pointerToolsGroup->addAction( m_addStateAction );

    m_mainToolbar->addActions( m_pointerToolsGroup->actions() );

    connect( m_pointerToolsGroup, SIGNAL( triggered( QAction * ) ),
             this, SLOT( onPointerToolsGroupClicked() ) );

    m_mainToolbar->addSeparator();

    m_stateChoiceWidget = new StateChoiceWidget( this );
    m_stateChoiceAction = new QWidgetAction( this );
    m_stateChoiceAction->setDefaultWidget( m_stateChoiceWidget );
    m_mainToolbar->addAction( m_stateChoiceAction );
    connect( m_stateChoiceWidget, SIGNAL( stateChanged( int ) ),
             this, SLOT( onChoiceWidgetStateChanged( int ) ) );
    connect( m_stateChoiceWidget, SIGNAL( choiceChanged( int ) ),
             this, SLOT( onChoiceWidgetStateChanged( int ) ) );
    onChoiceWidgetStateChanged( 0 );

    m_editAction = new QAction( QIcon::fromTheme( "accessories-text-editor" ),
                                tr( "&Edit Item Label" ),
                                this );
    m_editAction->setShortcut( QKeySequence( tr( "Ctrl+E", "Edit state/transition label" ) ) );
    m_mainToolbar->addAction( m_editAction );

    m_deleteAction = new QAction( QIcon::fromTheme( "edit-delete" ),
                                  tr( "&Delete Item" ),
                                  this );
    m_mainToolbar->addAction( m_deleteAction );


    QWidget *toolbarStretch = new QWidget( this );
    toolbarStretch->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
    m_mainToolbar->addWidget( toolbarStretch );

    QWidget *zoomWidget = new QWidget( this );
    zoomWidget->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Minimum );
    zoomWidget->resize( 150, 10 );
    QHBoxLayout *zoomWidgetLayout = new QHBoxLayout( zoomWidget );
    zoomWidget->setLayout( zoomWidgetLayout );
    zoomWidgetLayout->addWidget( new QLabel( tr( "Zoom " ), zoomWidget ) );
    m_zoomSlider = new QSlider( Qt::Horizontal, zoomWidget );
    zoomWidgetLayout->addWidget( m_zoomSlider );
    m_zoomBox = new QLabel( zoomWidget );
    zoomWidgetLayout->addWidget( m_zoomBox );
    zoomWidgetLayout->addWidget( new QLabel( "%", zoomWidget ) );
    connect( m_zoomSlider, SIGNAL( valueChanged( int ) ),
             m_zoomBox,    SLOT( setNum( int ) ) );
    connect( m_zoomSlider, SIGNAL( valueChanged( int ) ),
             this,         SLOT( changeZoomScale( int ) ) );
    m_zoomSlider->setRange( 20, 400 );
    m_zoomSlider->setValue( 100 );
    m_zoomSlider->setTickInterval( 20 );
    m_zoomSlider->setSingleStep( 10 );
    m_zoomSlider->setPageStep( 20 );

    m_mainToolbar->addWidget( zoomWidget );
}

void
MainWindow::createItemMenu()
{
    m_itemMenu = new QMenu( tr( "&Automaton" ), m_view );
    m_itemMenu->addAction( m_editAction );
    m_itemMenu->addAction( m_deleteAction );
}

void
MainWindow::createBottomBar()
{
    m_validatorWidget = new ValidatorWidget( this );
    m_validatorWidget->setFloatable( false );
    m_validatorWidget->setMovable( false );
    m_validatorWidget->setIconSize( QSize( 32, 32 ) );

    addToolBar( Qt::BottomToolBarArea, m_validatorWidget );

    m_validatorWidget->automatonChanged();
}


void MainWindow::closeEvent( QCloseEvent *e )
{
    if( m_isFileOpen )
        fileClose();

    if( m_isFileOpen )
        e->ignore();
    else
        QMainWindow::closeEvent( e );
}
